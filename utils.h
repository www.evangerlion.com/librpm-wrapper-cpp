#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <list>

namespace software { namespace utils {
/**
 * @brief purge_version_string 获取纯净的版本号, 去除非数字部分例如 1.1.0b => 1.1.0
 * @param version_c 含有杂质的版本号
 * @return 纯净的版本号，点分十进制版本号
 */
std::string purge_version_string(const char *version_c);

/**
 * @brief extract_versions 获取字符串中的版本列表
 * @param title_string titile字符串信息
 * @return 版本列表
 */
std::list<std::string> extract_versions(const char *title_string);

/**
 * @brief clean_name 格式化软件名称, 去除安装包名称中带有g_badpackages的信息
 * @param software_name 软件名称
 * @return 格式化后的软件名称
 */
std::string clean_name(const char *software_name);

/**
 * @brief soft_name 格式化软件名称，将软件名称可能包含的正则符号进行转义
 * @param sn 待格式化软件名称
 * @return 格式化之后的软件名称
 */
std::string soft_name(const char *sn);

/**
 * @brief is_valid_platforms 校验当前的 exploit 信息是否是当前平台
 * @param platform 平台名称
 * @return
 */
bool is_valid_platforms(const char *platform);

}}

#endif // UTILS_H
