
#include "loose_version.h"
#include "software.h"
#include "exploit.h"
#include "utils.h"

#include <boost/tokenizer.hpp>
#include <boost/regex.hpp>

#include <iostream>
#include <vector>
#include <fstream>
#include <inttypes.h>

using namespace std;

void check_exploit(const std::list<software::exploit::Exploit> &exploit_db)
{
    software::TSoftwareList soft_list = software::software_list();
    for (software::TSoftwareList::iterator iter = soft_list.begin(); iter != soft_list.end(); ++iter)
    {
        const software::SoftwarePackageInfos &infos = *iter;
        std::list<software::exploit::Exploit> results;
        // std::cout << "name: " << infos.m_software_name << ", version: " << infos.m_version << endl;
        if (search_exploit(infos.m_software_name.c_str(), infos.m_version.c_str(), exploit_db, results))
        {
            for (std::list<software::exploit::Exploit>::iterator iter = results.begin(); iter != results.end(); ++iter)
            {
                const software::exploit::Exploit &exploit = *iter;
                printf("[+]\033[0;32m %s\033[0;90m - %s\033[0m\n", exploit.exploit_description.c_str(), exploit.exploit_platform.c_str());
                printf("    From: %s %s\n", infos.m_software_name.c_str(), infos.m_version.c_str());
                printf("    File: /usr/share/exploitdb/%s\n", exploit.exploit_file_path.c_str());
                printf("    Url : https://www.exploit-db.com/exploits/%" PRId64 "\n", exploit.exploit_id);
                printf("    Type: %s\n", exploit.exploit_type.c_str());
                printf("    CVE : %s\n", exploit.exploit_cve_id.c_str());
            }
        }
    }
}

int main(int argc, char *argv[])
{
    std::string exploit_db_path = "linux-exploit.dat";
    if (argc >=2)
    {
        exploit_db_path = argv[1];
    }

    if (0 != ::access(exploit_db_path.c_str(), F_OK | R_OK))
    {
        std::cout << "not such file or unreadable, file: " << exploit_db_path << std::endl;
        return 1;
    }

    std::list<software::exploit::Exploit> g_exploit_db = software::exploit::load_exploits(exploit_db_path.c_str());

    check_exploit(g_exploit_db);
    return 0;
}
