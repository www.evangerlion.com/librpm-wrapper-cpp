#ifndef SOFTWARE_H
#define SOFTWARE_H

#include <string>
#include <list>

namespace software {

struct SoftwarePackageInfos
{
    std::string m_software_name;
    std::string m_version;
    std::string m_architecture;
    std::string m_description;
};
typedef std::list<SoftwarePackageInfos> TSoftwareList;

/**
 * @brief software_list
 * @return
 */
TSoftwareList software_list();

}
#endif // SOFTWARE_H
